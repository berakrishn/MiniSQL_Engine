import re,sys,csv,numpy,pandas,sqlparse

def main():
    global database_meta, query, database, distinct_flag
    database_meta = {}
    database = {}

    arguments = sys.argv[1:]
    count_arguments = len(arguments)
    if count_arguments == 1:
        ProcessMetaData()
        ProcessData()
        query = (sys.argv[1])
        ProcessQuery()
    else:
        print "invalid syntax\n usage: python mini_sql.py <sql_query>"

def ProcessQuery():
    #try:
    processed_sql_query = sqlparse.split(query)
    temp = []
    for itr in processed_sql_query:
        temp.append(sqlparse.format(itr,keyword_case="upper",identifier_case="lower",strip_comments=True))
    processed_sql_query = temp
    print processed_sql_query

    for sql_stmt in processed_sql_query:
        parsed_stmt = sqlparse.parse(sql_stmt)[0].tokens
        stmt_type = sqlparse.sql.Statement(parsed_stmt).get_type()
        identifier_list = []#sdfsdf ddfsdfsdfsdfsd sd sd fsdf sdf sdf s
        l = sqlparse.sql.IdentifierList(parsed_stmt).get_identifiers()
        for i in l:
            identifier_list.append(str(i))

        # Check for syntax errors in SQL here

        # Execute each SQL here
        # Branch according to the query
        if (stmt_type == 'SELECT'):
            try:
		        ProcessSelectQuery(parsed_stmt, identifier_list)
            except:
                print "error: invalid query"
                sys.exit(1)
        else:
            print "error: invalid query"

    #except:
    #    print >> sys.stderr, "error while parsing sql query\n please check input"
    #    sys.exit(1)

def CheckTableExistence(table):
    if table in database_meta:
        return True
    else:
        return False

def ErrorInvalidTable():
    print "error: invalid table name"

def CheckFieldExistence(cols,table):
    valid_columns_flag = False
    for itr in cols:
        itr = itr.upper()
        if itr in database_meta[table]:
            valid_columns_flag = True
        else:
            valid_columns_flag = False
    return valid_columns_flag

def ErrorInvalidField():
    print "error: invalid field name"

def ErrorInvalidAggregateArguments():
    print "error: invalid number of aggregate arguments"

def ErrorInvalidAggregateType():
    print "error: invalid aggregate type"

def CheckAggregateFunction(columns):
    arg = columns[0]
    aggregate_type_and_col = []
    if re.match(r'MAX\(.+\)',arg):
        tp = re.sub(r'MAX\(','',arg)
        tp = re.sub(r'\)','',tp)
        aggregate_type_and_col.append(True)
        aggregate_type_and_col.append(tp.lower())
        aggregate_type_and_col.append("MAX")
    elif re.match(r'MIN\(.+\)',arg):
        tp = re.sub(r'MIN\(','',arg)
        tp = re.sub(r'\)','',tp)
        aggregate_type_and_col.append(True)
        aggregate_type_and_col.append(tp.lower())
        aggregate_type_and_col.append("MIN")
    elif re.match(r'AVG\(.+\)',arg):
        tp = re.sub(r'AVG\(','',arg)
        tp = re.sub(r'\)','',tp)
        aggregate_type_and_col.append(True)
        aggregate_type_and_col.append(tp.lower())
        aggregate_type_and_col.append("AVG")
    elif re.match(r'SUM\(.+\)',arg):
        tp = re.sub(r'SUM\(','',arg)
        tp = re.sub(r'\)','',tp)
        aggregate_type_and_col.append(True)
        aggregate_type_and_col.append(tp.lower())
        aggregate_type_and_col.append("SUM")
    #print "Aggregate_fn => ",aggregate_type_and_col
    return aggregate_type_and_col

def ProcessSelectQuery(parsed_stmt, identifier_list):
    query_executed_flag = False
    distinct_flag = 0
    if 'DISTINCT' in identifier_list:
        distinct_flag = 1

    # select max/min/avg(col1) from table1
    columns_index = []
    columns = identifier_list[-3].split(',')
    columns = [x.upper() for x in columns]
    print identifier_list,columns

    if CheckTableExistence(identifier_list[-1]) == True:
        return_value = CheckAggregateFunction(columns)
        if len(return_value) == 0:
            ErrorInvalidAggregateType()
        elif return_value[0] == True:
            columns = []
            columns.append(return_value[1].upper())
            if CheckFieldExistence(columns,identifier_list[-1]) == True:
                if len(columns) == 1:
                    columns_index.append(database_meta[identifier_list[-1]].index(columns[0]))
                    #print "Cols, Cols_Index => ", columns, columns_index
                    if return_value[2] == 'MAX':
                        print "3->AGG>max"
                        print numpy.max(database[identifier_list[-1]][:,columns_index],axis=0)
                        query_executed_flag = True
                    elif return_value[2] == 'MIN':
                        print "3->AGG>min"
                        print numpy.min(database[identifier_list[-1]][:,columns_index],axis=0)
                        query_executed_flag = True
                    elif return_value[2] == 'AVG':
                        print "3->AGG>avg"
                        if distinct_flag == 0:
                            print "3->NON-DISTINCT"
                            print numpy.mean(database[identifier_list[-1]][:,columns_index],axis=0)
                        else:
                            print "3->DISTINCT"
                            print numpy.mean(numpy.unique(database[identifier_list[-1]][:,columns_index],axis=0))
                        query_executed_flag = True
                    elif return_value[2] == 'SUM':
                        print "3->AGG>sum"
                        if distinct_flag == 0:
                            print "3->NON-DISTINCT"
                            print numpy.sum(database[identifier_list[-1]][:,columns_index],axis=0)
                        else:
                            print "3->DISTINCT"
                            print numpy.sum(numpy.unique(database[identifier_list[-1]][:,columns_index],axis=0))
                        query_executed_flag = True
                elif len(columns) is not 1:
                    ErrorInvalidAggregateArguments()
            elif CheckFieldExistence(columns,identifier_list[-1]) == False:
                ErrorInvalidField()
    else:
        ErrorInvalidTable()

    # select * from table1
    if query_executed_flag is not True:
        select_all_wildcard = sqlparse.sql.Identifier(parsed_stmt).is_wildcard()
        print select_all_wildcard, identifier_list

        if select_all_wildcard == True:
            if CheckTableExistence(identifier_list[-1]) == True:
                if distinct_flag == 0:
                    print "1->NON-DISTINCT"
                    print database[identifier_list[-1]]
                else:
                    print "1->DISTINCT"
                    print numpy.unique(database[identifier_list[-1]],axis=0)
                query_executed_flag = True
            else:
                ErrorInvalidTable()

    # select col1, col2 from table1
    if query_executed_flag is not True:
        columns_index = []
        columns = identifier_list[-3].split(',')
        columns = [x.upper() for x in columns]
        print identifier_list,columns

        if CheckTableExistence(identifier_list[-1]) == True:
            if CheckFieldExistence(columns,identifier_list[-1]) == True:
                for itr in columns:
                    columns_index.append(database_meta[identifier_list[-1]].index(itr))
                print columns, columns_index
                if distinct_flag == 0:
                    print "2->NON-DISTINCT"
                    print database[identifier_list[-1]][:,columns_index]
                else:
                    print "2->DISTINCT"
                    print numpy.unique(database[identifier_list[-1]][:,columns_index],axis=0)
                query_executed_flag = True
            elif CheckFieldExistence(columns,identifier_list[-1]) == False:
                ErrorInvalidField()
        else:
            ErrorInvalidTable()

def ProcessMetaData():
    meta_data_file = open('metadata.txt','r')
    table_found = False
    table_name_found = False
    for itr in meta_data_file:
        if itr.strip() == "<begin_table>":
            #print "begin"
            table_found = True
            table_name_found = True

        elif table_found == True and table_name_found == True:
            #print "title",itr
            itr = itr.strip()
            database_meta[itr] = []
            table_name = itr
            table_name_found = False

        elif itr.strip() != "<end_table>":
            #print "not end",table_name
            database_meta[table_name].append(itr.strip())

        elif itr.strip() == "<end_table>":
            #print "end"
            table_found = False

    print database_meta

def ProcessData():
    for itr_key, itr_value in database_meta.iteritems():
        print itr_key, type(itr_key),"\t", itr_value
        database[itr_key] = []

        file_name = itr_key + '.csv'
        data_table = numpy.array(pandas.read_csv(file_name,header=None))
        database[itr_key] = data_table

    print database

if __name__ == "__main__":
    main()
